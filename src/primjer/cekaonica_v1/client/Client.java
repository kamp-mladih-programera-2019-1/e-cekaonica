    package primjer.cekaonica_v1.client;

    import java.io.*;
    import java.net.InetAddress;
    import java.net.Socket;
    import java.util.Scanner;

    public class Client {

        public static final int TCP_PORT = 9000;

        public static void main(String[] args) {
            System.out.println("Dobrodosli!\nIzaberite opciju: \n1 Licna karta\n2 Pasos");
            System.out.print("Unesite opciju: ");
            Scanner scan = new Scanner(System.in);
            Socket socket;
            BufferedReader in;
            PrintWriter out;
            try {
                Integer option;
                do {
                    option = scan.nextInt();
                } while (option != 1 && option != 2);
                    InetAddress addr = InetAddress.getByName("127.0.0.1");
                    socket = new Socket(addr, TCP_PORT);
                    in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
                    if (option == 1) {
                        out.println("ID");
                    }
                    else
                        out.println("PASSPORT");


                //Prima informacije (broj, broj ljudi ispred
                String receive = in.readLine();
                System.out.println(receive);
                scan.close();
                in.close();
                out.close();
                socket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
