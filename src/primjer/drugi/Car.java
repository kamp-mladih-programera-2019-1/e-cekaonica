package primjer.drugi;

public class Car extends Thread {
    private String type;
    private int id;
    //Prepoznaje cim prvo auto zavrsi
    private static boolean end = false;

    protected Car(String t, int id) {
        type = t;
        this.id = id;
    }

    public int getID() {
        return id;
    }

//Metoda koja opisuje ponasanje threada
    public void run() {
        for (int i = 0; i < 15; i++) {

            if (end) {
                System.out.println("Race ended " + type + ". You didn't win! Till the end: " + (14 - i) + ".");
                break;
            } else {

                //Provjeravamo da li je auto u polju sa znakom STOP
                if (Race.matrix[id][i].equals("STOP")) {


                    System.out.println("STOP sign for " + type + "!");
                    try {
                        //Uspavljujemo thread na 5s
                        sleep(5000);
                    } catch (InterruptedException e) {
                        System.out.println("Car didn't end first!");
                    }

                } else {
                    try {

                        System.out.println(type + " is driving. Till the end: " + (14 - i) + ".");

                        //Uspavljujemo thread na 2s
                        sleep(2000);
                    } catch (InterruptedException e) {
                        System.out.println("Car didn't end first!");
                    }

                }
                //Provjeravamo da li smo dosli do kraja staze
                if (i == 14) {
                    end = true;
                    System.out.println("WINNER  " + type);
                    //Zaustavljamo kad dodje do kraja
                    interrupt();
                }
            }

        }

    }
}
