package primjer.cekaonica_v1.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {
    // podesavamo port na kom ce server osluskivati zahtjeve od klijenata
    public static final int TCP_PORT = 9000;
    static Integer nextNumberID = 0;
    static Integer nextNumberPassport = 0;
    static Integer total = 0;

    static ArrayList<Socket> idSockets = new ArrayList();
    static ArrayList<Socket> passportSockets = new ArrayList();
    static boolean idFree = true;
    static boolean passportFree = true;

    public static Integer getNextNumberID() {return nextNumberID; }

    public static void main(String[] args) {
        try {
            // slusaj zahtjeve na datom portu
            BufferedReader in;
            ServerSocket ss = new ServerSocket(TCP_PORT);
            System.out.println("Server running...");
            while (true) {
                // prihvataj klijente
                Socket sock = ss.accept();
                PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(sock.getOutputStream())), true);
                in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                String request = in.readLine();
                System.out.println("req "+request);

                if ("ID".equals(request)) {
                //Obrada zahtjeva za licnu kartu
                    total++;
                    System.out.println("<<<SERVER>>>" + request + " ### Redni broj: " + total + ". Ispred Vas ima " + nextNumberID + " klijenata.");
                    out.println("### Redni broj: " + total + ". Ispred Vas ima " + nextNumberID + " klijenata.");
                    nextNumberID++;
                    idSockets.add(sock);

                } else if ("PASSPORT".equals(request)) {
                //Obrada zahtjeva za pasos
                    total++;
                    System.out.println("<<<SERVER>>> " + request + " ### Redni broj: " + total + ". Ispred Vas ima " + nextNumberPassport + " klijenata.");
                    out.println("### Redni broj: " + total + ". Ispred Vas ima " + nextNumberPassport + " klijenata.");
                    nextNumberPassport++;
                    passportSockets.add(sock);
                } else System.out.println("Unknown command!!");

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
