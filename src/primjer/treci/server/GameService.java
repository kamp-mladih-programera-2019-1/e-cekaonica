package primjer.treci.server;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class GameService {

    /**
     * metoda priprema 5 pitanja
     *
     * @return
     */
    public static synchronized ArrayList<String> prepareQuestions() {
        try {
            return new ArrayList<String>(
                    Files.readAllLines(Paths.get("src/primjer/treci/questions.txt")).stream().limit(5).collect(Collectors.toList()));

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return new ArrayList<String>();
    }
}
