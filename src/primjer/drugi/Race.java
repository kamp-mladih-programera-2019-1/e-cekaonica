package primjer.drugi;

import java.util.Random;

public class Race {

    static String[][] matrix;

    public static void main(String[] args) {
        matrix = new String[3][15];
        Random rnd = new Random();

        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 15; j++) {
                matrix[i][j] = "";
            }

        String stop = "STOP";
        for (int i = 0; i < 4; i++) {
            int position1 = rnd.nextInt(3);
            int position2 = rnd.nextInt(15);
            matrix[position1][position2] = stop;
            System.out.println("1:" + position1 + " 2:" + position2);
        }

        //Instanciramo tri auta
        Car first = new Car("BMW", 0);
        Car second = new Car("Mercedes", 1);
        Car third = new Car("Porsche", 2);
        //Pokrecemo threadove
        first.start();
        second.start();
        third.start();


    }
}
