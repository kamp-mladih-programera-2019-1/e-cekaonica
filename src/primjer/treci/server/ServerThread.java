package primjer.treci.server;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

public class ServerThread extends Thread {

    private Socket sock;
    private BufferedReader in;
    private PrintWriter out;
    private int score = 0;

    public ServerThread(Socket sock) {
        this.sock = sock;
        try {
            // inicijalizuj ulazni stream
            in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
            // inicijalizuj izlazni stream
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(sock.getOutputStream())), true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        start();
    }

    public void run() {
        try {
            ArrayList<String> questions = GameService.prepareQuestions();
            int currentQuestion = 0;
            String request;
            boolean isActive = true;

            while (isActive) {
                request = in.readLine();
                System.out.println("[Klijent " + sock.getInetAddress() + ":" + sock.getPort() + "] " + request);

                if ("QUESTION".equals(request)) {
                    out.println(questions.get(currentQuestion));
                } else if (request.startsWith("RESPONSE#")) {
                    String answer = request.split("#")[1];
                    String correctAnswer = questions.get(currentQuestion).split("#")[4];
                    if (answer.equals(correctAnswer)) {
                        score++;
                    }
                    currentQuestion++;
                } else if ("SCORE".equals(request)) {
                    out.println(score);
                }

              else  if ("END".equals(request)) {
                    isActive = false;
                } else {
                    out.println("UNKNOWN COMMAND");
                }
            }

            // zatvori konekciju
            in.close();
            out.close();
            sock.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.println("[Klijent " + sock.getInetAddress() + ":" + sock.getPort() + "] se odjavljuje.");
    }

}
