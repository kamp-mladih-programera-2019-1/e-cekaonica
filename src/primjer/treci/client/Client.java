package primjer.treci.client;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    // podesavamo port na kom cemo osluskivati
    public static final int TCP_PORT = 9000;

    public static void main(String args[]) {
        try {
            System.out.println("Kviz");
            Scanner scan = new Scanner(System.in);
            // odredi adresu racunara sa kojim se povezujemo
            // (povezujemo se sa nasim racunarom)
            InetAddress addr = InetAddress.getByName("127.0.0.1");
            // otvori socket prema drugom racunaru
            Socket sock = new Socket(addr, TCP_PORT);
            // inicijalizuj ulazni stream
            BufferedReader in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
            // inicijalizuj izlazni stream
            PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(sock.getOutputStream())), true);
            /*
             * klijent salje zahtjeve serveru u vidu poruka i ocitava njegove odgovore;
             * same akcije zavise od protokola komunikacije
             */
            for (int i = 0; i < 5; i++) {
                out.println("QUESTION");
                String item = in.readLine();
                String[] parts = item.split("#");
                if (parts.length < 4) {
                    throw new IllegalArgumentException("Greska pri komunikaciji");
                }
                String question = parts[0];
                String answer1 = parts[1];
                String answer2 = parts[2];
                String answer3 = parts[3];

                System.out.println(question);
                System.out.println("1 - " + answer1);
                System.out.println("2 - " + answer2);
                System.out.println("3 - " + answer3);
                System.out.println("Odgovor:");
                String response = scan.nextLine();
                System.out.println("=================================");
                out.println("RESPONSE#" + response);
            }
            out.println("SCORE");
            String score = in.readLine();
            System.out.println("Rezultat je: " + score);

            out.println("END");
            // zatvori konekciju
            scan.close();
            in.close();
            out.close();
            sock.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
